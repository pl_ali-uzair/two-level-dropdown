import { Component } from "@angular/core";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent {
  title = "two-level-dropdown";
  menus = [
    {
      title: "Home",
      link: "/home",
      children: []
    },
    {
      title: "Page 1",
      // link: "",
      children: [
        {
          title: "Page 1.1",
          // link: "",
          children: [
            {
              title: "Page 1.1.1",
              link: "page111",
              children: []
            },
            {
              title: "Page 1.1.2",
              link: "page112",
              children: []
            },
            {
              title: "Page 1.1.3",
              link: "page113",
              children: []
            }
          ]
        },
        {
          title: "Page 1.2",
          link: "page12",
          children: []
        },
        {
          title: "Page 1.3",
          link: "page13",
          children: []
        },
        {
          title: "Page 1.4",
          link: "page14",
          children: []
        }
      ]
    },
    {
      title: "Page 2",
      link: "page2",
      children: []
    },
    {
      title: "Page 3",
      // link: "",
      children: [
        {
          title: "Page 3.1",
          // link: "",
          children: [
            {
              title: "Page 3.1.1",
              link: "page311",
              children: []
            }
          ]
        },
        {
          title: "Page 3.2",
          // link: "",
          children: [
            {
              title: "Page 3.2.1",
              link: "page321",
              children: []
            }
          ]
        },
        {
          title: "Page 3.3",
          link: "page33",
          children: []
        },
      ]
    }
  ];
}

/*
    {
      title: "Page 1",
      link: "",
      children: []
    },
*/
