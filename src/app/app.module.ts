import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { HomeComponent } from "./home/home.component";
import { Page111Component } from "./pages/page111/page111.component";
import { Page112Component } from "./pages/page112/page112.component";
import { Page113Component } from "./pages/page113/page113.component";
import { Page12Component } from "./pages/page12/page12.component";
import { Page13Component } from "./pages/page13/page13.component";
import { Page14Component } from "./pages/page14/page14.component";
import { Page2Component } from "./pages/page2/page2.component";
import { Page311Component } from "./pages/page311/page311.component";
import { Page321Component } from "./pages/page321/page321.component";
import { Page33Component } from './pages/page33/page33.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    Page111Component,
    Page112Component,
    Page113Component,
    Page12Component,
    Page13Component,
    Page14Component,
    Page2Component,
    Page311Component,
    Page321Component,
    Page33Component
  ],
  imports: [BrowserModule, AppRoutingModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
