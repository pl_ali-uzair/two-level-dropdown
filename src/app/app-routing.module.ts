import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { Page111Component } from "./pages/page111/page111.component";
import { Page112Component } from "./pages/page112/page112.component";
import { Page113Component } from "./pages/page113/page113.component";
import { Page12Component } from "./pages/page12/page12.component";
import { Page13Component } from "./pages/page13/page13.component";
import { Page14Component } from "./pages/page14/page14.component";
import { Page2Component } from "./pages/page2/page2.component";
import { Page311Component } from "./pages/page311/page311.component";
import { Page321Component } from "./pages/page321/page321.component";
import { Page33Component } from './pages/page33/page33.component';

const routes: Routes = [
  {path: "", component: HomeComponent},
  {path: "home", component: HomeComponent},
  {path: "page111", component: Page111Component},
  {path: "page112", component: Page112Component},
  {path: "page113", component: Page113Component},
  {path: "page12", component: Page12Component},
  {path: "page13", component: Page13Component},
  {path: "page14", component: Page14Component},
  {path: "page2", component: Page2Component},
  {path: "page311", component: Page311Component},
  {path: "page321", component: Page321Component},
  {path: "page33", component: Page33Component},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
